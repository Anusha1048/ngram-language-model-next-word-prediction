import nltk
from nltk import word_tokenize
from nltk.util import ngrams
from collections import Counter

text = "The temple was demolished and a vast, Classical-style church was built around Golgotha (the hill where Jesus’s crucifixion was believed to have taken place).He bought several Strip properties — the Silver Slipper and Castaways among them — and demolished them to make way for a new kind of resort — Mirage — which became an instant success.It was demolished in 1817."
token = nltk.word_tokenize(text)
bigrams = ngrams(token,2)
trigrams = ngrams(token,3)
fourgrams = ngrams(token,4)
fivegrams = ngrams(token,5)

# print("**")
# print(bigrams)
# print("***")
# print(Counter(trigrams))
# print("****")
# print(Counter(fourgrams))
# print("****")
counter_fivegram=Counter(fivegrams).keys()

# counter_bigram=Counter(bigrams)
# print(counter_bigram)
n=4
m=-4
myNgrams=None
message = "The temple was demolished from the".split()
# messages=message[m:]
# print(messages)
print(len(message))

def my_ngrams(message,n):
    myNgrams = [message[i:i + n] for i in range((len(message) - n) + 1)]
    # print(myNgrams)
    return myNgrams
myNgrams=my_ngrams(message,n)
# print(myNgrams)
# for key in Counter(fivegrams).keys():
#     print(key)
tuple_list = [tuple(lst) for lst in myNgrams]
n_grams_list=[]
for n_gram in counter_fivegram:
    count=0
    for i_word, n_word in zip(tuple_list[-1], n_gram):
        if(i_word==n_word):
            print(i_word,n_word)
            count=count+1
            print(count)
    if(count==3):
        n_grams_list.append(n_gram)
print(n_grams_list)
if tuple_list[-1] in Counter(bigrams).keys():
        print("yes")
