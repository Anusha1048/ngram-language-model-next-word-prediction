import re
import spacy
import numpy as np
import collections
nlp = spacy.load('en_core_web_sm')
with open('corpus.txt', 'r') as cf:
    corpus = []
    for line in cf: # loops over all the lines in the corpus
        line = line.strip() # strips off \n \r from the ends
        if line: # Take only non empty lines
            line = re.sub(r'\([^)]*\)', '', line) # Regular Expression to remove text in between brackets
            line = re.sub(' +',' ', line) # Removes consecutive spaces
            # add more pre-processing steps
            corpus.append(line)
# print("\n".join(corpus[:5]))
def preprocess_corpus(corpus):
    corpus_tokens = []
    sentence_lengths = []
    for line in corpus:
        doc = nlp(line) # Parse each line in the corpus
        for sent in doc.sents: # Loop over all the sentences in the line
            print(sent)
            corpus_tokens.append('SEQUENCE_BEGIN')
            s_len = 1
            for tok in sent: # Loop over all the words in a sentence
                if tok.text.strip() != '' and tok.ent_type_ != '': # If the token is a Named Entity then do not lowercase it
                    corpus_tokens.append(tok.text)
                else:
                    corpus_tokens.append(tok.text.lower())
                s_len += 1
            corpus_tokens.append('SEQUENCE_END')
            sentence_lengths.append(s_len+1)
    return corpus_tokens, sentence_lengths

corpus_tokens, sentence_lengths = preprocess_corpus(corpus[:5])
print(corpus_tokens)
print(sentence_lengths)
# print(corpus_tokens[:30])
mean_sentence_length = np.mean(sentence_lengths)
deviation_sentence_length = np.std(sentence_lengths)
max_sentence_length = np.max(sentence_lengths)
print('Mean Sentence Length: {}\nSentence Length Standard Deviation: {}\n'
      'Max Sentence Length: {}'.format(mean_sentence_length, deviation_sentence_length, max_sentence_length))
vocab = list(set(corpus_tokens))
print(vocab)


word_counter = collections.Counter()
for term in corpus_tokens:
    word_counter.update({term: 1})
vocab = word_counter.most_common(10000) # 10000 Most common terms
print('Vocab Size: {}'.format(len(vocab)))
print(word_counter.most_common(100))


vocab.append(('UNKNOWN', 1))
print(vocab)
Idx = range(1, len(vocab)+1)
vocab = [t[0] for t in vocab]
print(vocab)


Word2Idx = dict(zip(vocab, Idx))
Idx2Word = dict(zip(Idx, vocab))
# print(Word2Idx)
# print(Idx2Word)


Word2Idx['PAD'] = 0
Idx2Word[0] = 'PAD'
print(Word2Idx)
print(Idx2Word)
VOCAB_SIZE = len(Word2Idx)
print('Word2Idx Size: {}'.format(len(Word2Idx)))
print('Idx2Word Size: {}'.format(len(Idx2Word)))


w2v = np.random.rand(len(Word2Idx), 300)
print(w2v)



for w_i, key in enumerate(Word2Idx):
    token = nlp(key)
    if token.has_vector:
        #print(token.text, Word2Idx[key])
        # w2v[Word2Idx[key]:] = token.vector
        continue
print(w2v)
EMBEDDING_SIZE = w2v.shape[-1]
print('Shape of w2v: {}'.format(w2v.shape))
print('Some Vectors')
print(w2v[0][:10], Idx2Word[0])
print(w2v[80][:10], Idx2Word[80])

train_val_split = int(len(corpus_tokens) * 0.8) # We use 80% of the data for Training and 20% for validating
train = corpus_tokens[:train_val_split]
validation = corpus_tokens[train_val_split:-1]

print('Train Size: {}\nValidation Size: {}'.format(len(train), len(validation)))