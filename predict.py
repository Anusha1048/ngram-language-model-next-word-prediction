import nltk
from collections import Counter
from nltk.util import ngrams
import operator
user_text=input()
user_text=user_text.split()
print(user_text)
text = "I am aware that nltk only but was there for  offers I am aware that nltk only bigrams I am aware that nlts only and trigrams, but was there from a way to split my text in four-grams,but was there from five-grams or but was there even but was then hundred-grams"
tokenize = nltk.word_tokenize(text)
unigrams=ngrams(tokenize,1)
bigrams = ngrams(tokenize,2)
trigrams=ngrams(tokenize,3)
fourgrams=ngrams(tokenize,4)
fivegrams=ngrams(tokenize,5)

unigrams=Counter(unigrams)
bigrams=Counter(bigrams)
trigrams=Counter(trigrams)
fourgrams=Counter(fourgrams)
fivegrams=Counter(fivegrams)
# print(bigrams)
# print(trigrams)
# print(fourgrams)
unigram_prob_dict={}
total_unigrams=sum(list(unigrams.values()))
for word,fre in unigrams.items():
    unigram_prob_dict[word]=fre/total_unigrams
# print(unigram_prob_dict)
biagram_prob_dict={}
total_biagrams=sum(list(bigrams.values()))
for words,fre in bigrams.items():
    biagram_prob_dict[words]=fre/total_biagrams
# print(biagram_prob_dict)
trigram_prob_dict={}
total_trigrams=sum(list(trigrams.values()))
for words,fre in trigrams.items():
    trigram_prob_dict[words]=fre/total_biagrams
# print(trigram_prob_dict)
fourgrams_prob_dict={}
total_fourgrams=sum(list(fourgrams.values()))
for words,fre in fourgrams.items():
    fourgrams_prob_dict[words]=fre/total_fourgrams
# print(fourgrams_prob_dict)
fivegrams_prob_dict={}
total_fivegrams=sum(list(fivegrams.values()))
for key,fre in fivegrams.items():
    fivegrams_prob_dict[key]=fre/total_fivegrams
# print(fivegrams_prob_dict)
def check_bigrams(user_text,biagram_prob_dict):
    user_text=user_text[-1]
    # print((user_text))
    matching_bigrams={}
    # print("**")
    for key,prob in biagram_prob_dict.items():
        if user_text == key[0]:
            matching_bigrams[key]=prob
    # print(matching_bigrams)
    sorted_bigrams = dict( sorted(matching_bigrams.items(), key=operator.itemgetter(1),reverse=True))
    # print(sorted_bigrams)
    suggestions=[]
    if sorted_bigrams.keys():
        suggestions=list(sorted_bigrams.keys())[0]
    # print(suggestions[1])
    if suggestions:
        return suggestions[1]
    else:
        return suggestions
def check_trigrams(user_text,trigram_prob_dict):
    user_text=user_text[-2:]
    # print(user_text)
    matching_trigrams={}
    for key,prob in trigram_prob_dict.items():
        tri_gram=[]
        count=0
        for i in range(len(key)-1):
            tri_gram.append(key[i])
        # print(tri_gram)
        # print("start")
        for i_word,n_word in zip(user_text,tri_gram):
            if i_word==n_word:
                count=count+1
            # print(count)
        if(count==2):
            matching_trigrams[key]=prob
    # print(matching_trigrams)
    sorted_trigrams = dict(sorted(matching_trigrams.items(), key=operator.itemgetter(1), reverse=True))
    # print(sorted_trigrams)
    suggestions=[]
    if sorted_trigrams.keys():
        suggestions = list(sorted_trigrams.keys())[0]
    if suggestions:
        # print(suggestions[2])
        return  suggestions[2]
    else:
        return  suggestions
def check_fourgrams(user_text,fourgrams_prob_dict):
    user_text=user_text[-3:]
    # print(user_text)
    matching_fourgrams = {}
    for key, prob in fourgrams_prob_dict.items():
        four_gram = []
        count = 0
        for i in range(len(key) - 1):
            four_gram.append(key[i])
        # print(tri_gram)
        # print("start")
        for i_word, n_word in zip(user_text, four_gram):
            if i_word == n_word:
                count = count + 1
            # print(count)
        if (count == 3):
            matching_fourgrams[key] = prob
    # print(matching_fourgrams)
    sorted_fourgrams = dict(sorted(matching_fourgrams.items(), key=operator.itemgetter(1), reverse=True))
    # print(sorted_fourgrams)
    suggestions=[]
    if sorted_fourgrams.keys():
        suggestions = list(sorted_fourgrams.keys())[0]
    if suggestions:
        return suggestions[3]
    else:
        return suggestions
def check_fivegrams(user_text,fivegrams_prob_dict):
    user_text=user_text[-4:]
    print(user_text)
    suggestion = []
    matching_fivegrams = {}
    matching_chargrams={}
    final_h_prob_chargrams={}
    i_chars=[]
    n_chars=[]
    for key, prob in fivegrams_prob_dict.items():
        five_gram = []
        count = 0
        for i in range(len(key) - 1):
            five_gram.append(key[i])
        # print(tri_gram)
        # print("start")
        for i_word, n_word in zip(user_text, five_gram):
            if count == 3:
                i_chars.append(i_word)
                n_chars.append(n_word)
                matching_chargrams[key]=prob
            if i_word == n_word:
                count = count + 1
            # print(count)

        if (count == 4):
            matching_fivegrams[key] = prob

        if (count==3):
                # matching_chargrams = dict(sorted(matching_chargrams.items(), key=operator.itemgetter(1), reverse=True))
                for i_word,n_word,(key,prob) in zip(i_chars,n_chars,matching_chargrams.items()):
                    # print(i_word,n_word,key)
                    # print(key,prob)
                    count=0
                    for i_char,n_char in zip(i_word,n_word):
                        if i_char==n_char:
                            count=count+1
                    if(count==len(i_word)):
                        final_h_prob_chargrams[key]=prob

                # if suggestion:
                #     print(suggestion[3])
                #     # return suggestions[4]
                # return suggestion

    print(matching_fivegrams)

    print(final_h_prob_chargrams)
    sorted_final_chargrams = sorted(final_h_prob_chargrams.items(), key=operator.itemgetter(1), reverse=True)
    print("%")
    print(sorted_final_chargrams)
    suggestion=sorted_final_chargrams[0]
    print(suggestion[0][3])
    # suggestion = []
    # if sorted_final_chargrams.keys():
    #     suggestion = list(sorted_final_chargrams.keys())
    #     # print(suggestion)
    #
    # sorted_fivegrams = sorted(matching_fivegrams.items(), key=operator.itemgetter(1), reverse=True)
    # print(sorted_fivegrams)
    # suggestions=[]
    # if sorted_fivegrams.keys():
        # suggestions = list(sorted_fivegrams.keys())
    # if suggestions:
        # print(suggestions[4])
        # return suggestions[4]
    return suggestions

suggestions=[]
if len(user_text):
    if not suggestions and len(user_text)>=4:
        print("4")
        suggestions = check_fivegrams(user_text, fivegrams_prob_dict)
        print(suggestions)
    if not suggestions and len(user_text)>=3:
        print("3")
        suggestions = check_fourgrams(user_text, fourgrams_prob_dict)
        print(suggestions)

    if not suggestions and len(user_text)>=2:
        print("2")
        suggestions = check_trigrams(user_text, trigram_prob_dict)
        print(suggestions)

    if not suggestions and len(user_text)>=1:
        print("1")
        suggestions = check_bigrams(user_text,biagram_prob_dict)
        print(suggestions)



# print(suggestions)