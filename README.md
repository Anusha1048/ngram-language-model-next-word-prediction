Algorithm:-

1.create frequency tables for bigrams,trigrams,quadgrams,fivegrams such that the first column holds the (n-1)-gram, second column the word completing the n-gram and third column contains the frequency

2.when given an sentence for which the next word should be predicted, select last 5 words (call this group “t”) subset fivegram table on column 1 so that only rows matching t are available; select row with highest frequency in this subset and return column 2 (completion) word

3.if no matching subset available in fivegram table, remove first word from t and subset fourgram table on t-1 so that only rows matching t-1 are available. As with fivegram table; select row with highest frequency in this subset and return column 2 (completion) word4.if no match in fourgrams table then check in trigrams and bigrams.

Limitations:-

1.The higher the N, the better is the model usually. But this leads to lots of computation overhead that requires large computation power in terms of RAM

2.N-grams are a sparse representation of language. This is because we build the model based on the probability of words co-occurring. It will give zero probability to all the words that are not present in the training corpus(Out of vocabulary words)